package test.connexion;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.inject.Inject;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

public abstract class Environnement extends ActionSupport implements SessionAware {

    private Map<String, Object> variablesSession;
   /* private GestionDemineur facade;

   @Inject("FacadeDemineur")
    public void setFacadeDemineur(GestionDemineur gestionDemineur){
        this.gestionDemineur = gestionDemineur;
    }

    Attention au nom donné à la facade dans le strusts.xml
    */

   //TODO mettre en place la fonction de la facade en fonction du modèle donné


    public Map<String, Object> getVariablesSession() {
        return this.variablesSession;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.variablesSession = map;
    }

}